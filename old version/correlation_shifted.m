%% correlation for SDSS (shifted rp value to get same plots as in the paper)
%% initial setting

clc; clear; close all;

Nreg=50;
Nbin=10; % here use 1..10 instead of 0..9 in Ishak summationChi.c
RMIN=0.0001;
EFACT=log(60/0.3)/Nbin; % instead of log(100)/10 in Ishak summationChi.c
COSMIN=cos(atan((60/3000)/0.0494)); % ~0.9269, this is COSMAX in Ishak summationChi.c
dChiMAX=0.02; % 60/3000
H0=3000; % unit Mpc/h where h=0.7
R=0.87;

%rp=[0.49 0.9 1.9 3.7 6.4 10.05 15 24 41 60];
rp=[0.48 0.96 1.9 3.9 6.4 10.0 17 25 40.5 60];
%rp=H0*RMIN*exp((1:Nbin)*EFACT);
%rp=[ 0.5096    0.8656    1.4704    2.4977    4.2426    7.2067   12.2417   20.7943   35.3222   60.0000];
%% generate Chi(z) interpolation

% N=10000;
% Chi(N*7)=0;
% syms x;
% for i=1:N*7
%     Chi(i)=integral( @(x) (0.3*(1+x).^3+0.7).^-0.5 , 0, i/N); % note haven't *H0^-1 (=3000 Mpc) yet
% end;

%% read SDSS data

cd('C:\Users\Ji\Desktop\Research\Limber integral Weak Lensing\Codes\reproduce Mandelbaum');
load L6.dat;
alpha=L6(:,1)/180*pi; % convert deg->rad
delta=L6(:,2)/180*pi;
z=L6(:,3);
chi=z.*(1-0.225*z);
% chi=Chi(fix(N*z))'; % comoving distance;
e1=L6(:,11);
e2=L6(:,12);
reg=L6(:,17)+1; % jack-kinfe region 0..49 -> 1..50
REmatrix=[alpha delta e1 e2]; % matrix for rotellip.m

NG=length(alpha);

%% find pairs & calculate estimators in SDSS

SpD(Nbin)=0;
ED(Nbin,Nreg)=0;

SpSp(Nbin)=0;
EE(Nbin,Nreg)=0;

SxSx(Nbin)=0;
E45E45(Nbin,Nreg)=0;

Npairs(1:Nbin)=0;
binpairs(Nbin,Nreg)=0;

ux=cos(delta).*cos(alpha); % get 3D unit vector for each galaxy
uy=cos(delta).*sin(alpha);
uz=sin(delta);

for i=1:NG
    cossep=ux*ux(i)+uy*uy(i)+uz*uz(i);
    sep12=0.5*(chi(i)+chi).*acos(cossep); % (zi+zj)/2*theta
    %ibins=1+fix( log(sep12/RMIN) / EFACT );
    ibins=Nbin+1+zeros(length(sep12),1);
    for j=Nbin:-1:1
        ibins(find(sep12<rp(j)/H0))=j;
    end;
    ibins(find(sep12<0.3/H0))=0;
    slt=find( COSMIN<cossep & chi(i)-dChiMAX<chi & chi<chi(i)+dChiMAX & 1<=ibins & ibins<=Nbin ); % within angle and redshift range
    [e1i e2i]=rotellip(REmatrix,i,slt);
    [e1j e2j]=rotellip(REmatrix,slt,i);
    ibins=ibins(slt);
    
    ipairs=length(slt);
    for j=1:ipairs
        
        bin=ibins(j);
        
        Npairs(bin)=Npairs(bin)+1;
        
        SpD(bin)=SpD(bin)+e1i(j);
        SpSp(bin)=SpSp(bin)+e1i(j)*e1j(j);
        SxSx(bin)=SxSx(bin)+e2i(j)*e2j(j);
        
        jk=setdiff(1:Nreg,[reg(i),reg(slt(j))]);
        binpairs(bin,jk)=binpairs(bin,jk)+1;
        ED(bin,jk)=ED(bin,jk)+e1i(j);
        EE(bin,jk)=EE(bin,jk)+e1i(j)*e1j(j);
        E45E45(bin,jk)=E45E45(bin,jk)+e2i(j)*e2j(j);
        
    end;
end;

SpD=SpD/(2*R);
ED=ED/(2*R);
SpSp=SpSp/(2*R)^2;
EE=EE/(2*R)^2;
SxSx=SxSx/(2*R)^2;
E45E45=E45E45/(2*R)^2;
DD=Npairs;

sigma_SpD(Nbin)=0;
sigma_SpSp(Nbin)=0;
sigma_SxSx(Nbin)=0;
for i=1:Nbin
    sigma_SpD(i)=sqrt( sum(( ED(i,:)./binpairs(i,:)*Npairs(i) - SpD(i) ).^2) );
    sigma_SpSp(i)=sqrt(sum(( EE(i,:)./binpairs(i,:)*Npairs(i) - SpSp(i) ).^2));
    sigma_SxSx(i)=sqrt(sum(( E45E45(i,:)./binpairs(i,:)*Npairs(i) - SxSx(i) ).^2));
end;

%% read random catalog

load RL6.dat;
alpha=RL6(:,1)/180*pi; % convert deg->rad
delta=RL6(:,2)/180*pi;
z=RL6(:,3);
chi=z.*(1-0.225*z);
%chi=Chi(fix(N*z))'; % comoving distance;
e1=RL6(:,11);
e2=RL6(:,12);
reg=RL6(:,17)+1; % jack-kinfe region 0..49 -> 1..50
REmatrix=[alpha delta e1 e2]; % matrix for rotellip.m

NG=length(alpha);

%% find pairs & calculate estimators in random catalog

SpR(Nbin)=0;
ER(1:Nbin,1:Nreg)=0;

RNpairs(1:Nbin)=0;
Rbinpairs=zeros(Nbin,Nreg); % to calculate error for RR
regcount(Nreg)=0;
for i=1:Nreg
    regcount(i)=length( find(reg==i) ); % ngalaxies_reg in Ishak summationChiR.c
end;

ux=cos(delta).*cos(alpha); % get 3D unit vector for each galaxy
uy=cos(delta).*sin(alpha);
uz=sin(delta);

for i=1:NG
    cossep=ux*ux(i)+uy*uy(i)+uz*uz(i);
    sep12=0.5*(chi(i)+chi).*acos(cossep); % (zi+zj)/2*theta
    %ibins=1+fix( log(sep12/RMIN) / EFACT );
    ibins=Nbin+1+zeros(length(sep12),1);
    for j=Nbin:-1:1
        ibins(find(sep12<rp(j)/H0))=j;
    end;
    ibins(find(sep12<0.3/H0))=0;
    slt=find( COSMIN<cossep & chi(i)-dChiMAX<chi & chi<chi(i)+dChiMAX & 1<=ibins & ibins<=Nbin ); % within angle and redshift range
    [e1i e2i]=rotellip(REmatrix,i,slt);
    [e1j e2j]=rotellip(REmatrix,slt,i);
    ibins=ibins(slt);
    
    ipairs=length(slt);
    for j=1:ipairs
        
        bin=ibins(j);
        
        RNpairs(bin)=RNpairs(bin)+1;
        
        SpR(bin)=SpR(bin)+e1i(j);
        
        jk=setdiff(1:Nreg,[reg(i),reg(slt(j))]);
        Rbinpairs(bin,jk)=Rbinpairs(bin,jk)+1;
        ER(bin,jk)=ER(bin,jk)+e1i(j);
        
    end;
end;

SpR=SpR/(2*R);
ER=ER/(2*R);
RR=RNpairs;

sigma_SpR(Nbin)=0;
for i=1:Nbin
    %sigma_SpR(i)=sqrt(sum(( ED(i,:)./binpairs(i,:)*Npairs(i) - SpR(i) ).^2));
    sigma_RR(i)=sqrt(sum(( Rbinpairs(i,:)./(NG-regcount)*NG-RR(i) ).^2));
end;

%% get correlations from estimators

%rp=H0*RMIN*exp((1:Nbin)*EFACT);

wgp=2*H0*dChiMAX*(SpD)./RR;
for i=1:Nbin
    sigma_wgp1(i)=sqrt( sum( ( (ED(i,:)./binpairs(i,:)*Npairs(i)) ./ (Rbinpairs(i,:)./(NG-regcount)*NG) *2*H0*dChiMAX-wgp(i) ).^2 ));
end;
sigma_wgp=wgp.*(sigma_SpD./SpD+sigma_RR./RR);
sigma_wgp2=wgp.*(((sigma_SpD./SpD).^2+(sigma_RR./RR).^2).^0.5);

wpp=2*H0*dChiMAX*SpSp./RR;
sigma_wpp=2*H0*dChiMAX*sigma_SpSp./RR;

wxx=2*H0*dChiMAX*SxSx./RR;
sigma_wxx=2*H0*dChiMAX*sigma_SxSx./RR;
%% plot

%figure('visible','off');
errorbar(rp,wgp.*sqrt(rp)*0.1,sigma_wgp.*sqrt(rp)*0.1,'ko');
set(gca,'xscale','log');
hold on;
errorbar(rp,wgp.*sqrt(rp)*0.1,sigma_wgp1.*sqrt(rp)*0.1,'ro');
errorbar(rp,wgp.*sqrt(rp)*0.1,sigma_wgp2.*sqrt(rp)*0.1,'bo');
plot([0.1 100],[0 0]);
hold off;

% saveas(gcf,'wgp','pdf');

figure;
errorbar(rp,wpp.*rp*.1,sigma_wpp.*rp*.1,'ko');
set(gca,'xscale','log');
hold on;
plot([0.1 100],[0 0]);
hold off;

figure;
errorbar(rp,wxx.*rp*.1,sigma_wxx.*rp*.1,'ko');
set(gca,'xscale','log');
hold on;
plot([0.1 100],[0 0]);
hold off;
