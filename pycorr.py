import time

time0=time.time()

def chi(z,omegam=0.27,H=100):
  # import cosmology
  # c0=cosmology.Cosmo(H0=H,omega_m=omegam)
  # return c0.Dc(0.,z)
  from astropy import cosmology
  from astropy.cosmology import FlatLambdaCDM
  cosmo = FlatLambdaCDM(H0=70, Om0=0.27)
  return cosmo.comoving_distance(z).value

import numpy as np

data=np.genfromtxt('L6.dat',dtype=None,names=['ra','dec','z','temp1','temp2','temp3','temp4','temp5','temp6','temp7','e1','e2','tem1','tem2','tem3','tem4','reg'])
rands=np.genfromtxt('RL6.dat',dtype=None,names=['ra','dec','z','temp1','temp2','temp3','temp4','temp5','temp6','temp7','e1','e2','tem1','tem2','tem3','tem4','reg'])

datar=chi(data['z'])
randsr=chi(rands['z'])
points=np.ones(len(datar))

rmin = -60.
rmax = 60.

import treecorr

dataE=treecorr.Catalog(g1=data['e1'], g2=data['e2'], ra=data['ra'], dec=data['dec'], r=datar, ra_units='deg', dec_units='deg')
dataP=treecorr.Catalog(ra=data['ra'], dec=data['dec'], r=datar, ra_units='deg', dec_units='deg')
randP=treecorr.Catalog(ra=rands['ra'], dec=rands['dec'], r=randsr, ra_units='deg', dec_units='deg')

ED=treecorr.NGCorrelation(nbins=10, min_sep=.3/.7, max_sep=60./.7, bin_slop=0.01, verbose=0, min_rpar=rmin/.7, max_rpar=rmax/.7)
ER=treecorr.NGCorrelation(nbins=10, min_sep=.3/.7, max_sep=60./.7, bin_slop=0.01, verbose=0, min_rpar=rmin/.7, max_rpar=rmax/.7)
RR=treecorr.NNCorrelation(nbins=10, min_sep=.3/.7, max_sep=60./.7, bin_slop=0.01, verbose=0, min_rpar=rmin/.7, max_rpar=rmax/.7)

ED.process(dataP,dataE,metric='Rperp')
ER.process(randP,dataE,metric='Rperp')
RR.process(randP,randP,metric='Rperp')

xi_gp=(ED.xi*ED.npairs-ER.xi*ER.npairs)/RR.npairs/(2*0.87)
wgp=xi_gp*120
error_wgp=np.sqrt( ED.varxi*(ED.npairs/RR.npairs)**2 + ER.varxi*(ER.npairs/RR.npairs)**2 )*120/(2*0.87)
detection=np.sqrt( sum((wgp/error_wgp)**2) /9 )

print wgp
print error_wgp
print ED.meanr
print 'detection= ',detection

np.savetxt('pycorr.out',(wgp,error_wgp,ED.meanr))

# import matplotlib
# matplotlib.use('Agg')
#
# from matplotlib.pyplot import errorbar,savefig
#
# errorbar(ED.meanr,xi_gp,error_xi,'o')
# savefig('pycorr.pdf')

import matplotlib

from matplotlib import pyplot as plt

fig1=plt.figure()
plt.semilogx()
plt.errorbar(ED.meanr,wgp*np.sqrt(ED.meanr),error_wgp*np.sqrt(ED.meanr),fmt='o')
plt.plot(ED.meanr,np.zeros(10),color='k')
plt.xlabel('$r_p$')
plt.ylabel('$w_{g+}\sqrt{r_p}$')
# plt.savefig('pycorr_plus.png')

print time.time()-time0
plt.show()
