load workspace;
rp=rp/0.7; % convert unit Mpc/h => Mpc
load pycorr.out;
x=pycorr(1,:);
dx=pycorr(2,:);
rperp=pycorr(3,:);

figure;
errorbar(rperp,sqrt(rperp).*x,sqrt(rperp).*dx,'ko');
set(gca,'xscale','log');
hold on;
plot([0.1 100],[0 0]);
hold off;


% to compare
figure;errorbar(rp,wgp.*sqrt(rp),sigma_wgp.*sqrt(rp),'ko');hold on;errorbar(rperp,sqrt(rperp).*x,sqrt(rperp).*dx,'ro');plot([0.1 100],[0 0]);hold off; set(gca,'xscale','log');
lgd{1}='correlation.m  max(r_p)';
lgd{2}='pycorr.py  mean(r_p)';
legend(lgd);
saveas(gcf,'pycorr','pdf');
